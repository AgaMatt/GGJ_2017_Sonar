﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grades : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Sonar")
        {
          //  print("ocorreu o trigger com o sonar");
            StartCoroutine(FadeTo(2.5f, 1.5f));
        }
        else if (other.gameObject.tag == "Player") {
            print("ocorreu o trigger com o player");
        }
    }

    void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "Player") {
            StartCoroutine(FadeTo(2.5f, 1.5f));
        }
    }

    IEnumerator FadeTo(float aValue, float aTime)
    {
        float alpha = GetComponent<SpriteRenderer>().color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime) // contador para o tempo de execucao
        {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, aValue, t));
            GetComponent<SpriteRenderer>().color = newColor;
            yield return null;
        }

        if (GetComponent<SpriteRenderer>().color.a == 0f)
        {
            StopCoroutine("FadeTo");
        }
        else
        {
            StartCoroutine(obstaculoOff());
        }
    }

    IEnumerator obstaculoOff()
    {
        yield return new WaitForSeconds(5);
        StartCoroutine(FadeTo(0.0f, 1.0f));
    }
}
