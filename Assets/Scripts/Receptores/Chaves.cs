﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chaves : MonoBehaviour
{
    public GameObject porta;
    public AudioSource audioPortaSpace;
    public EnemyAlarm _EnemyAlarm;
    public Sprite[] framePorta;
    public GameObject[] SensorMovimento;
    public Interface _interfaceGame;

    void Awake()
    {
        Color newColor = new Color(1, 1, 1, 0);
        GetComponent<SpriteRenderer>().color = newColor;
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Sonar")
        {
                _interfaceGame.objetivoTxt.text = "Find the New Way";
                audioPortaSpace.Play();
                StartCoroutine(FadeTo(2.5f, 1.5f));
                coll.gameObject.GetComponent<Collider2D>().isTrigger = true;
               // porta.gameObject.GetComponent<BoxCollider2D>().enabled = false;
                porta.gameObject.GetComponent<SpriteRenderer>().sprite = framePorta[1];
                foreach(GameObject sensor in SensorMovimento)
                {
                _EnemyAlarm.IsEnable = false;     
                }
                porta.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;

        }
    }

    IEnumerator FadeTo(float aValue, float aTime)
    {
        float alpha = GetComponent<SpriteRenderer>().color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime) // contador para o tempo de execucao
        {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, aValue, t));
            GetComponent<SpriteRenderer>().color = newColor;
            yield return null;
        }

        if (GetComponent<SpriteRenderer>().color.a == 0f)
        {
            StopCoroutine("FadeTo");
        }
        else
        {
            StartCoroutine(obstaculoOff());
        }

    }

    IEnumerator obstaculoOff()
    {
        yield return new WaitForSeconds(5);
        StartCoroutine(FadeTo(0.0f, 1.0f));
    }
}