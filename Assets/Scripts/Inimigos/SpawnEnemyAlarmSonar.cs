﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemyAlarmSonar : MonoBehaviour {
	public GameObject pivotSonarAlarm;
	// Use this for initialization
	void Start () {
		Instantiate (pivotSonarAlarm, transform.position, Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
