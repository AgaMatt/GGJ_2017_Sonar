﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interface : MonoBehaviour {

    public Text gameOverText, objetivoTxt, tutorialTxt;
    public Image[] vidas;
    public int qtdVida = 3;
    public ingameInterface _IngameInterface;
	CriarSonar _criarSonar;

    public Personagem _personagemScript;

	void Start()
	{
		_criarSonar = FindObjectOfType<CriarSonar> ();
		//print (_criarSonar.name);
	}

	void Update()
	{
		if(_criarSonar.FirstSonarReleased)
		{
			tutorialTxt.enabled = false;
		}
	}

    public void mudancaVida()
    {
        qtdVida -= 1;

        switch (qtdVida) {
            case 2:
                vidas[0].gameObject.SetActive(false);
                break;
            case 1:
                vidas[1].gameObject.SetActive(false);
                break;
            case 0:
                vidas[2].gameObject.SetActive(false);
                break;
        }

        if (qtdVida == 0) {
            gameOverText.text = "You lost all lives \n Try again!";
            _personagemScript.gamePaused = true;
            Time.timeScale = 0f;
			_IngameInterface.finalGame();
        }
    }

	public void mundancaVidaTotal() {
        qtdVida = 0;
        gameOverText.text = "You lost all lives \n Try again!";
        _personagemScript.gamePaused = true;
        Time.timeScale = 0f;
        _IngameInterface.finalGame();
    }

}
