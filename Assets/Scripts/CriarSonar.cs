﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CriarSonar : MonoBehaviour 
{
    public bool IsActive {get; private set; }
	public bool IsAllowed { get; private set; }
	public bool FirstSonarReleased { get; private set;}

    public bool isGamePaused;
	private int alarmBool;

	private List<Transform> sonarList;
	private Animator anim;

	public GameObject pivotSonar;

    void Awake()
    {
        IsActive = true;
        IsAllowed = true;
		FirstSonarReleased = false;
	}
    public void InitVoice() 
	{
      //  print("ALOPESSOAL");
		anim.SetBool ("SonarOn", true);
		IsActive = true;
		FirstSonarReleased = true;
		StartCoroutine(initSondas());
    }

	void Start() 
	{
		anim = GetComponent<Animator>();
		sonarList = new List<Transform>();

		foreach (Transform child in pivotSonar.GetComponentInChildren<Transform>())
		{
			sonarList.Add (child);
		}
	}

	void Update() 
	{

        if (Input.GetKeyUp(KeyCode.Space))
        {
           // print("ASEAEW");
            if(IsAllowed)
            InitVoice();
        }
        if (IsActive) 
		{
			return;
		}

		
	}

    IEnumerator initSondas() 
	{
		if (IsAllowed)
        {
            Instantiate(pivotSonar, gameObject.transform.GetChild(0).position, Quaternion.identity);
			IsAllowed = false;
        }

        yield return new WaitForSeconds(3);
		IsActive = false;
        anim.SetBool("SonarOn", false);
        IsAllowed = true;
    }		
}


