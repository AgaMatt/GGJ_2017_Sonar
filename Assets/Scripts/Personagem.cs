﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Personagem : MonoBehaviour{

public float speed = 3.0f;
//private float movementSpeedSonar = 1.0f;
public bool sonarAtivo = false;
// public Transform pivotSonar;
public GameObject[] sonar = new GameObject[0];

public bool gamePaused;

public AudioClip shootSound;
private AudioSource source;

public int nextLevel;
//private float volLowRange = .5f;
//private float volHighRange = 1.0f;

void Awake()
{

	source = GetComponent<AudioSource>();

}

void Update()
{

	/*if (gamePaused == false)
        {

            if (sonarAtivo == true)
            {
                sonar[0].transform.Translate(Vector3.up * movementSpeedSonar * Time.deltaTime);
                sonar[1].transform.Translate(Vector3.right * movementSpeedSonar * Time.deltaTime);
                sonar[2].transform.Translate(Vector3.down * movementSpeedSonar * Time.deltaTime);
                sonar[3].transform.Translate(Vector3.left * movementSpeedSonar * Time.deltaTime);
                // Direcionais
                sonar[4].transform.Translate(Vector3.up * movementSpeedSonar * Time.deltaTime);
                sonar[4].transform.Translate(Vector3.right * movementSpeedSonar * Time.deltaTime);

                sonar[5].transform.Translate(Vector3.down * movementSpeedSonar * Time.deltaTime);
                sonar[5].transform.Translate(Vector3.right * movementSpeedSonar * Time.deltaTime);

                sonar[6].transform.Translate(Vector3.down * movementSpeedSonar * Time.deltaTime);
                sonar[6].transform.Translate(Vector3.left * movementSpeedSonar * Time.deltaTime);

                sonar[7].transform.Translate(Vector3.up * movementSpeedSonar * Time.deltaTime);
                sonar[7].transform.Translate(Vector3.left * movementSpeedSonar * Time.deltaTime);
            }*/

	// movimentacao personagem
	var move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
	transform.position += move * speed * Time.deltaTime;

	/*if (Input.GetKeyUp("space"))
            {
                pivotSonar.transform.position = transform.position;
                for (int i = 0; i < 8; i++)
                {
                    sonar[i].transform.position = transform.position;
                    sonar[i].SetActive(true);
                }
                float vol = Random.Range(volLowRange, volHighRange);
                source.PlayOneShot(shootSound, vol);
                sonarAtivo = true;
            }*/
}

void OnCollisionEnter2D(Collision2D other) { 
	if (other.gameObject.name == "colliderFinalGame") {
		SceneManager.LoadScene(3);
	}
}
}


    
